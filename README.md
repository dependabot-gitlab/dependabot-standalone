# dependabot-standalone

Pipeline configuration that runs [dependabot-gitlab](https://gitlab.com/dependabot-gitlab/dependabot) for dependency
updates via gitlab scheduled pipelines.

[[_TOC_]]

# Issues

All issues and bug reports should be submitted in the main [dependabot-gitlab/dependabot](https://gitlab.com/dependabot-gitlab/dependabot/-/issues) repository

# Setup

## Authentication

Minimal setup requires GitLab personal access token with `api`, `read_repository` access scope and at least `Developer` role. It is strongly suggested to also have GitHub personal access token with repository read scope to avoid rate limiting.

- For CI/CD component setup, tokens must be provided as inputs to the component. Inputs support variable expansion so it can be names of environment variables from CI/CD configuration.

- For non CI/CD component setup, gitlab and github access tokens must be provided as environment variables. Best practice is to use [gitlab environment variables](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui) feature. Following variables must be set:

  - `SETTINGS__GITLAB_ACCESS_TOKEN` - gitlab personal access token
  - `SETTINGS__GITHUB_ACCESS_TOKEN` - github personal access token

## dependabot.yml

Dependency update behaviour is configured via [.gitlab/dependabot.yml](https://dependabot-gitlab.gitlab.io/dependabot/config/configuration.html) file. Every target project must have this file for dependency updates to work.

## .gitlab-ci.yml setup

By default dependency update jobs target project that is running the jobs. To target different project, set `PROJECT_PATH` environment variable to project path, like `dependabot-gitlab/dependabot`.

Additional custom application configuration can be provided via environment variables. For a full list of environment variables supported, refer to [environment variables](https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html) configuration section.

### Via CI/CD component

To use `CI/CD component` on self-hosted instance, this repository needs to be mirrored on that instance: <https://docs.gitlab.com/ci/components/#use-a-gitlabcom-component-on-gitlab-self-managed>

Update jobs can be added using [CI/CD component](https://docs.gitlab.com/ci/components/#use-a-component) feature.

Add following to `.gitlab-ci.yml` file:

```yml
include:
  - component: $CI_SERVER_FQDN/dependabot-gitlab/dependabot-standalone/template@3.44.0
    inputs:
      gitlab_access_token: gitlab-access-token
      github_access_token: github-access-token
    rules:
      - if: $PACKAGE_MANAGER_SET
```

For a list of all inputs, refer to [spec.inputs](templates/template.yml) section of the template file.

#### Dependency proxy

To use [dependency proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/) for docker images, override `dependabot_gitlab_image` input like so:

```yml
include:
  - component: ...
    inputs:
      dependabot_gitlab_image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/andrcuns/dependabot-gitlab
```

#### Job stage

Optionally override stage where dependabot jobs are executed (`test` stage by default)

```yml
include:
  - component: ...
    inputs:
      stage_name: dependabot

stages:
  - ...existing stages...
  - dependabot
```

### Via Include

**This method is deprecated and will be removed in future versions.** Use [CI/CD component](#via-cicd-component) method instead.

- Include pipeline template directly:

```yml
include:
  - project: dependabot-gitlab/dependabot-standalone
    file: .gitlab-ci.yml
    ref: 3.44.0
    rules:
      - if: $PACKAGE_MANAGER_SET
```

#### Dependency proxy

To use [dependency proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/) for docker images, override `DEPENDABOT_GITLAB_IMAGE` variable like so:

```yml
variables:
  DEPENDABOT_GITLAB_IMAGE: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/andrcuns/dependabot-gitlab
```

#### Job stage

Optionally override stage where dependabot jobs are executed (`test` stage by default)

```yml
stages:
  - ...existing stages...
  - dependabot

.dependabot-gitlab:
  stage: dependabot
```

### Fork

- Fork the repository and setup scheduled jobs in the fork

### Manual

- Copy job definitions from [.gitlab-ci.yml](.gitlab-ci.yml) in to your own `.gitlab-ci.yml` file.

**Note:** Every ecosystem requires it's own specific docker image, take note of how full image name is constructed.

## CI/CD => Schedules configuration

All update jobs are executed via gitlab scheduled pipelines. To setup scheduled pipelines, navigate to `CI/CD => Schedules` section of the project and add new schedule. Following variables must be set:

- `PACKAGE_MANAGER_SET` - comma separated package eco-systems, like `bundler,docker` (dependency file must be in the same directory for multiple package managers to work within same schedule job)
- `DIRECTORY` - update directory, usually `/` if not a larger monorepo

**Combination of `PACKAGE_MANAGER_SET` and `DIRECTORY` must exist in `dependabot.yml` file, these values do not override configuration, but act as a pointer for specific entry in `dependabot.yml` file.**

### Using configuration with multiple directories

`dependabot.yml` configuration supports [directories](https://dependabot-gitlab.gitlab.io/dependabot/config/configuration.html#directories). In order to use it, configuration entry must define unique `name` key. The name of this key must be set via `CONFIG_ENTRY_NAME` variable in the scheduled pipeline.

## Disabling dependency updates

To temporarily disable dependency updates without modifying .gitlab-ci.yml file, set `DEPENDENCY_UPDATES_DISABLED` variable to `true` in `CI/CD => Variables` section of the project.
